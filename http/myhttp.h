#ifndef _MY_HTTP_H_
#define _MY_HTTP_H_

#include <stdio.h>
#include "event.h"

void add_params(char *buffer,const char *key,const char *value);

int  updateSensor(char *buffer);

void cleanBuffer(char *buffer);

void transportBMEdata(int idx,float iaq,float temperature,float humidity,float pressure);

#endif

